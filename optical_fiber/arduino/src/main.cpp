#define WEBSERVER_H
#include <WiFiManager.h>
#include <FastLED.h>

#include "ConfigServer.h"

#include <ESP8266mDNS.h>

#include "OTA.h"
#include "Display.h"

ConfigServer configServer;
WiFiManager wifiManager;

#ifndef THINGNAME
#define THINGNAME thing
#endif

class OpticalDisplay : public Display {
    public: 
        int opposite(int led) {
            return (led + ((sideSize * 3 - 1) - (led%sideSize) * 2))%(sideSize*4);
        }

    private:
        int sideSize = 16;
};

class Animation {
    public:
        Animation(OpticalDisplay *display) {
            this->display = display;
        }
        virtual void animate() = 0;

    protected:
        OpticalDisplay *display;
};

class PlainRainbow : public Animation {
    public:
        PlainRainbow(OpticalDisplay *display) : Animation(display) {}
        void animate() {
            display->clear();
            for (int led = 0; led < display->nbLeds; led++) {
                display->setLed(led, CHSV(hue, 255, 255));
            }
            
            hue = (hue + 1)%255;
            delay(10);
            display->show();
        }

    private:
        byte hue = 0;
};

class OppositeRainbow : public OpticalDisplay {
    public:
        void loop() {
            clear();
            for (int led = 0; led < nbLeds/2; led++) {
                CRGB color = CHSV(255/(nbLeds/2) * led + hue, 255, 255);
                setLed(led, color);
                setLed((led + nbLeds/2)%nbLeds, color);
                hue = (hue + 1)%255;
            }
            delay(100);
            FastLED.show();
        }

    private:
        byte hue = 0;
};


class Accelerating : public Animation {
    public:
        Accelerating(OpticalDisplay *display) : Animation(display) {
            hues = new byte[display->nbLeds]();
        }
        void animate() {

            int offLeds = 48;
            hues[led] =  hue;
            
            display->setLed(led, CHSV(hues[led], 255, 255));
            
            for (int i = 1; i < offLeds + 1; i++) {
                display->setLed(((led + i)%display->nbLeds), 
                    CHSV(hues[(led + i)%display->nbLeds], 255, 0));
            }

            for (int i = offLeds + 1 ; i < display->nbLeds; i++) {
                display->setLed(((led + i)%display->nbLeds), 
                    CHSV(hues[(led + i)%display->nbLeds], 
                         255, 
                         (i*255)/(display->nbLeds - offLeds)));
            }

            hue = (hue + 2) % 255;
            led = (led + 1) % display->nbLeds;

            if( (led%(display->nbLeds)) == 0) {
                speed = speed + speedDir;
                if((speed <= 1) || (speed > 20)) {
                    speedDir = speedDir * -1;
                }
            }

            display->show();
            if(speed > 0) {
                delay(speed);
            }
        }

    private:
        byte *hues;
        int led = 0;
        byte hue = 0;
        int speed = 20;
        int speedDir = -2;
};

class Chasers : public Animation {
    public:
        Chasers(OpticalDisplay *display, int minChaser = 2, int maxChaser = 16, int chaserStep = 1): Animation(display){
            hues = new byte[display->nbLeds];
            values = new byte[display->nbLeds];
            this->minChaser = minChaser;
            this->maxChaser = maxChaser;
            this->nbChaserStep = chaserStep;
            this->nbChaser = minChaser;
        }

        void animate() {
            int trailLength = (display->nbLeds/nbChaser)/2;

            for(int i = display->nbLeds - 2; i >= 0; i--) {
                values[i+1] = values[i];
                hues[i+1] = hues[i];
            }

            hues[0] = (hues[1] + hueStep)%255;

            if(led % (display->nbLeds/nbChaser) == 0) {
                values[0] = 255;
            } else {
                values[0] = max(0, values[1] - 255/trailLength);
            }

            for(int i = 0; i < display->nbLeds; i ++) {
                display->setLed(i, CHSV(hues[i], 255, values[i]));
            }

            led = (led + 1) % display->nbLeds;
            
            if(led == 0) {
                if(minChaser < maxChaser) {
                    revolution = revolution + 1;
                    if(revolution%(nbChaser/2 + 1) == 0) {
                        revolution = 0;
                        if(nbChaserStep > 0 ) {
                            nbChaser = nbChaser*2;
                        } else {
                            nbChaser = nbChaser/2;
                        }
                        
                        if((nbChaser >= maxChaser || nbChaser <= minChaser)) {
                            nbChaserStep = nbChaserStep * -1;
                        }
                    }
                }
            }
            display->show();
            delay(speed);
        }

    private:
        byte *hues;
        byte hue = 0;
        int hueStep = 3;
        byte *values;
        int led = 0;
        int speed = 18;
        int speedStep = 0;
        int nbChaser;
        int nbChaserStep;
        int minChaser;
        int maxChaser;
        int revolution = 0;
};

class FixedChaser : public Chasers {
    public:
        FixedChaser(OpticalDisplay *display, int nbChaser): Chasers(display, nbChaser, nbChaser, 0){ }
};

class Random : public Animation {
    public:
        Random(OpticalDisplay *display) : Animation(display) {
            values = new byte[display->nbLeds];
        }

        void animate() {
            
            for(int i=0; i<display->nbLeds; i++) {
                values[i] = max(0, values[i] - 255/32);
            }
            byte rand = random(display->nbLeds);
            values[rand] = 255;
            values[(rand+display->nbLeds/2)%display->nbLeds] = 255;

            for(int i=0; i<display->nbLeds; i++) {
                display->setLed(i, CHSV((i*255/display->nbLeds + offset)%255, 255, values[i]));
                values[i] = max(0, values[i] - display->nbLeds/16);
            }
            offset = (offset + 1)%255;
            display->show();

            delay(20);
        }
    
    private:
        byte *values;
        int offset = 0;
};

class RandomHalfColor : public Animation {
    public:
        RandomHalfColor(OpticalDisplay *display) : Animation(display) {
            hues = new byte[display->nbLeds];
            values = new byte[display->nbLeds];
        }

        void animate() {
            for(int i=0; i<display->nbLeds; i++) {
                values[i] = max(0, values[i] - 255/32);
            }
            byte rand = random(display->nbLeds);
            values[rand] = 255;

            byte color = random(255);
            hues[rand] = color;

            for(int i=0; i<display->nbLeds; i++) {
                display->setLed(i, CHSV(hues[i], 255, values[i]));
                values[i] = max(0, values[i] - display->nbLeds/16);
            }
            offset = (offset + 1)%255;
            display->show();

            delay(20);
        }
    
    private:
        byte *hues;
        byte *values;
        int offset = 0;
};

class RandomColor : public Animation {
    public:
        RandomColor(OpticalDisplay *display) : Animation(display){
            hues = new byte[display->nbLeds];
            values = new byte[display->nbLeds];
        }

        void animate() {
            
            for(int i=0; i<display->nbLeds; i++) {
                values[i] = max(0, values[i] - 255/32);
            }
            byte rand = random(display->nbLeds);
            values[rand] = 255;
            values[(rand+display->nbLeds/2)%display->nbLeds] = 255;

            byte color = random(255);
            hues[rand] = color;
            hues[(rand+display->nbLeds/2)%display->nbLeds] = color;

            for(int i=0; i<display->nbLeds; i++) {
                display->setLed(i, CHSV(hues[i], 255, values[i]));
                values[i] = max(0, values[i] - display->nbLeds/16);
            }
            offset = (offset + 1)%255;

            display->show();

            delay(20);
        }
    
    private:
        byte *hues;
        byte *values;
        int offset = 0;
};

class RandomWhite : public Animation {
    public:
        RandomWhite(OpticalDisplay *display) : Animation(display){
            values = new byte[display->nbLeds];
        }

        void animate() {
            
            for(int i=0; i<display->nbLeds; i++) {
                values[i] = max(0, values[i] - 255/32);
            }

            if(random(8) == 0) {
                byte rand = random(display->nbLeds);
                values[rand] = 255;
                values[(rand+display->nbLeds/2)%display->nbLeds] = 255;
            }
            
            for(int i=0; i<display->nbLeds; i++) {
                display->setLed(i, CRGB(values[i], values[i], values[i]));
                values[i] = max(0, values[i] - display->nbLeds/16);
            }
            offset = (offset + 1)%255;

            display->show();

            delay(20);
        }
    
    private:
        byte *values;
        int offset = 0;
};

class Rainbow : public Animation {
    public:
        Rainbow(OpticalDisplay *display) : Animation(display){}

        void animate() {
            for(int i=0; i < display->nbLeds; i++) {
                display->setLed(i, CHSV(i*255/display->nbLeds + hue, 255, 255));
            }

            hue = (hue + 4)%255;

            display->show();

            delay(10);
        }
    
    private:
        byte hue;
};

OpticalDisplay *display = new OpticalDisplay();

Animation *animation[] = {
    new Accelerating(display),
    new RandomColor(display),
    new Chasers(display),
    new Random(display),
    new PlainRainbow(display),
    new RandomWhite(display),
    new FixedChaser(display, 4), 
    new Rainbow(display)
};


int current = 0;
int nbAnimation = 8;

const int ANIMATION_DURATION = 6000;

OTA ota(display);

void setup() {
    Serial.begin(115200);
    wifiManager.autoConnect("optical");

    display->fill(CRGB::Blue);
    configServer.on("/firmware", HTTP_POST, ota.onRequest(), ota.onUpload());
    display->fill(CRGB::Yellow);
    configServer.begin();  
    display->fill(CRGB::DarkGreen);

    if (MDNS.begin(THINGNAME)) {
        MDNS.addService("http", "tcp", 80);
    } else {
        Serial.println("Error setting up MDNS responder!");
    }

    display->fill(CRGB::Black);
}

int lastChange = 0;

void loop() {
    MDNS.update();
    ota.loop();
    if(ota.isUpdateInProgress()) {
        delay(500);
        return;
    }
    
    if((millis() - lastChange) > ANIMATION_DURATION) {
        lastChange = millis();
        current = (current + 1)%nbAnimation;
        display->clear();
    }
    
    animation[current]->animate();
}