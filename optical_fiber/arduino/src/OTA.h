#ifndef __HTTP_OTA_H__
#define __HTTP_OTA_H__

#include <ArduinoOTA.h>
#include <ESPAsyncWebServer.h>
#include "OTAFeedbackDisplay.h"

class OTA {
    public:
        OTA(OTAFeedbackDisplay *feedback) {
            this->feedback = feedback;
            basicOTABegin();
        }
        ArRequestHandlerFunction onRequest();
        ArUploadHandlerFunction onUpload();
        void loop();
        boolean isUpdateInProgress() {
            return inProgress;
        }


    private:
        OTAFeedbackDisplay *feedback;
        boolean reboot = false;
        boolean inProgress = false;
        void basicOTABegin();
        void basicOTAHandle();
        void updateStart();
        void updateProgress(int progress);
        void updateEnd();
        void updateFail();
};

void OTA::loop() {
    basicOTAHandle();
    if(reboot) {
        Serial.println("OTA: Will reboot now");
        delay(500);
        ESP.reset();
    }
}

void OTA::updateStart() {
    Serial.println("OTA: updateStart");
    inProgress=true;
    feedback->updateInProgress(0);
}

void OTA::updateProgress(int progress) {
    Serial.printf("Progress: %u%%\r", progress);
    inProgress=true;
    feedback->updateInProgress(progress);
}

void OTA::updateEnd() {
    Serial.println("OTA: updateEnd");
    inProgress=false;
    feedback->updateEnded();
}

void OTA::updateFail() {
    Serial.println("OTA: updateFail");
    inProgress=false;
    feedback->updateFailed();
}

void OTA::basicOTABegin() {
    ArduinoOTA.onStart([&]() {
        String type;
        if (ArduinoOTA.getCommand() == U_FLASH) {
            type = "sketch";
        } else { // U_FS
            type = "filesystem";
        }
        this->updateStart();
    });

    ArduinoOTA.onEnd([&]() {
        this->updateEnd();
    });

    ArduinoOTA.onProgress([&](unsigned int progress, unsigned int total) {
        this->updateProgress(progress / (total / 100));
    });

    ArduinoOTA.onError([&](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
            Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
            Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
            Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
            Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
            Serial.println("End Failed");
        }
        updateFail();
    });

    ArduinoOTA.begin();
};

void OTA::basicOTAHandle() {
    ArduinoOTA.handle();
};

ArRequestHandlerFunction OTA::onRequest() {
    return [&](AsyncWebServerRequest *request){
                if (Update.hasError()) {
                    request->send(200, F("text/html"), String(F("Update error")));
                    updateFail();
                } else {
                    request->send(200, "text/html", "Firmware update ok, will reboot now.");
                    reboot = true;
                }
            };
}

ArUploadHandlerFunction OTA::onUpload() {
    return [&](AsyncWebServerRequest *request, const String& filename, size_t index, uint8_t *data, size_t len, bool final) {
        uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
        if(0 == index) {
            updateStart();
            Update.runAsync(true);
            if(!Update.begin(maxSketchSpace)) { 
                Serial.println("Update begin failure!"); 
                updateFail();
            }
        }

        if(Update.write(data, len) != len){
            Update.printError(Serial);
        } else {
            updateProgress((Update.progress()*100)/request->contentLength());
        }
        
        if(final) {
            if (Update.end(true)) {
                updateEnd();
            } else {
                Update.printError(Serial);
                updateFail();
            }
        }
    };
}

#endif