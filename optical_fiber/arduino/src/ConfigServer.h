#ifndef __CONFIG_SERVER_H__
#define __CONFIG_SERVER_H__

#include <ESP8266WebServer.h>
#define WEBSERVER_H
#include <ESPAsyncWebServer.h>


class ConfigServer {

    public:

    void begin();
    void on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest, ArUploadHandlerFunction onUpload);
    void on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest);
    
    private:
    AsyncWebServer server = AsyncWebServer(80);

    void configureCORS(String origin);
};


void ConfigServer::begin() {
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/plain", "Hello, world");
    });

    configureCORS("*");

    server.begin();
}


void ConfigServer::configureCORS(String origin) {
    DefaultHeaders::Instance().addHeader("Access-Control-Allow-Origin", origin);
    server.onNotFound([](AsyncWebServerRequest *request) {
        if (request->method() == HTTP_OPTIONS) {
            request->send(200);
        } else {
            request->send(404);
        }
    });
}


void ConfigServer::on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest, ArUploadHandlerFunction onUpload) {
    server.on(uri, method, onRequest, onUpload);
}


void ConfigServer::on(const char* uri, WebRequestMethodComposite method, ArRequestHandlerFunction onRequest) {
    server.on(uri, method, onRequest);
}

#endif