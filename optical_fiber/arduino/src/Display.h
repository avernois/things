#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#define NUM_LEDS 64
#define DATA_PIN 2

#include <FastLED.h>
#include "OTAFeedbackDisplay.h"


class Display : public OTAFeedbackDisplay {
    public:
    const int nbLeds = NUM_LEDS;

    Display() {
        FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
    }

    void updateInProgress(int progress) {
            for(int led =0; led < nbLeds; led++) {
                if((led)*100/nbLeds <= progress)
                    this->leds[led] = CRGB(40, 40, 0);
                else
                    this->leds[led] = CRGB::Black;
            }
            FastLED.show();
        };

        void updateEnded() {
            fill(CRGB(0, 30, 0));
            FastLED.show();
        };

        void updateFailed() {
            fill(CRGB(30, 0, 0));
            FastLED.show();
        };

        void setLed(int led, CRGB color) {
            this->leds[led] = color;
        }

        void fill(CRGB color) {
            for (int led =0; led < this->nbLeds; led++) {
                this->leds[led] = color;
            }
            FastLED.show();
        }

        void clear() {
            for (int led =0; led < this->nbLeds; led++) {
                this->leds[led] = CRGB::Black;
            }
        }

        void show() {
            FastLED.show();
        }

        private: 
            
            CRGB leds[NUM_LEDS];
};

 #endif