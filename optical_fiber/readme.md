# Optical fiber

This directory contains some experiments with optical fiber and leds.

## hyperbolic paraboloid

![Demo](images/2022.08.06-optical.1-h264.mp4)

 This is inspired by [@GeekMomProjects](https://twitter.com/GeekMomProjects) [hyperbolic paraboloid] (https://www.geekmomprojects.com/fiber-optic-hyperbolic-paraboloid/).

 ### Materials

* 3mm plywood (I used 3 A4 sheets)
* Almost 10m of 3mm side glow optical fiber
* 64 ws2812b leds from a 74 leds/m strip in 4 strips of 16 leds
* wire to connect the strips
* wood glue
* a 3 pin connectors (to connect the led strip to the controller)
* ESP8266 to control the led. The code provided is meant for the pcb that can be found on the [clock project](https://gitlab.com/avernois/clocks/-/tree/main/pcb/clock_ws2812b)
* (optional) CA glue
* (optional) 7 pcb [led strip connectors](https://gitlab.com/avernois/clocks/-/tree/main/pcb/connector_ws2812b)

### Tools

* solder iron
* small clamps

### Cutting wood structure

All pieces required are in [hyperbolic_paraboloid.svg](./svg/hyperbolic_paraboloid.svg).
There are meant to be cut in 3mm (1/8") wood (I used birch plywood).

In the [a4cut](svg/a4cut/) directory you'll find files I used for the cut. They are (manually) optimized for A4 plywood sheets.

### Building

![building pieces](images/2022.08.05-hyperbolic_pieces.600px.png)

Sadly I did not took picture while building it, but it should not be to difficult. In the following, numbers in ***bold italic*** represent a piece of the building as shown in the above picture.

1. glue two ***2*** together. Make sure they are well aligned and pay attention to not put glue in the holes (or you will have trouble to insert the optic fiber). Repeat 3 times. Let's call ***8*** those new assembly.
2. insert a ***6*** in each rectangular hole of a ***8***. In a row, all ***6*** should be in the same direction. It make it easier to glue them, but it is not strictly necessary. Repeat 3 times.
3. now it's time to connect the 4 ***8*** + ***6*** using ***1***. It's very easy to get the angle wrong, so I strongly recommand to first build the whole structure without glue, then once the build is complete, remove a ***1***, add some glue and put it make in place and repeat until 4 ***1*** are glued.
4. time to add some optic fiber. Holes should be tight enough to hold the optic fiber in place. But once you're happy with your threading, you can secure it with some CA glue.
5. cut excess of optic fiber, flush or up to 1 mm
6. add ***3*** on top
7. add a strip of 16 leds on each ***3***. Make sure they are oriented correctly so that they could be chained.
8. add *4* on top, and secure the assembly by inserting ***7*** in ***6***
9. solder wire to the strip in order to create a chain
10. solder wires with a connector at the begining of the chain.
11. once everything works fine, complete the structure by adding ***5*** and ***5'*** (note: technically, they are the same)

### Code

Code is found in [arduino](/arduino). It is meant for an ESP8266 with the data line of the led strip connected on pin 2.

It uses [platformio](https://platformio.org/) to manage build and dependencies. If you're not familiar with it, please [read their documentation](https://docs.platformio.org/en/latest/) to see how it works and how you can install/use it on your system.

### Next

I'd like to redesign side, to get them thinner without losing structural strength nor the ability to easily access the led strip.