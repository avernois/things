# lamp shade: pentagonal hexecontahedron

design for a pentagonal hexecontahedron (60 faces).
There are 150 edges, so 300 tenons are needed for a full polyhedron or 295 + 10 (small ones) for the shade (with a truncated vertice).

![pentagonal hexecontahedron, top view](lamp_shade/pentagonal_hexecontahedron/images/pentagonal_hexecontahedron.top.1024.jpg)
![pentagonal hexecontahedron](lamp_shade/pentagonal_hexecontahedron/images/pentagonal_hexecontahedron.1024.jpg)