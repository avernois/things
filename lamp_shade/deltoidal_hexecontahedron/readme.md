# lamp shade: deltoidal hexecontahedron

design for a deltoidal hexecontahedron (60 deltoidal faces) ceilling light shade.
There are 120 edges, so 240 tenons are needed for a full polyhedron or 235 + 10 (small ones) for the shade (with a truncated vertex).

![deltoidal hexecontahedron](lamp_shade/deltoidal_hexecontahedron/images/deltoidal_hexecontahedron.1024.jpg)