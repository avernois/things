# light

this is meant to be a big white light. It's an attempt to improve lighting when taking picture of my [led clocks](https://gitlab.com/avernois/clocks).

It's made for white leds strip with 60leds/m (cutable every 6leds or 10cm).
It requires 18 strips of 12 leds (or 20cm).