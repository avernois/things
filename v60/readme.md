# V60

This is a small stand to hold v60 coffee filter (size 01).

It is meant to be cut in 3mm plywood.

![V60 filter holder empty](v60/images/2021.05.24-v60_filter_holder.jpg)
![V60 filter holder with filter](v60/images/2021.05.24-v60_filter_holder-with_filters.jpg)